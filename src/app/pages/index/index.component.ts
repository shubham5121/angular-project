import { Component, OnInit } from '@angular/core';
import { OmdbService } from 'src/app/services/omdb.service';
import Movie from 'src/app/models/Movie';
import { MovieService } from 'src/app/services/movie.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  constructor(private omdbService: OmdbService, private movieService: MovieService) { }

  public saved: Movie[];
  public seen: Movie[];

  ngOnInit(): void {
    this.movieService.getLatestSaved(3).subscribe((movies: Movie[]) => {
      this.saved = movies;
    })
    this.movieService.getLatestSeen(3).subscribe((movies: Movie[]) => {
      this.seen = movies;
    })
  }

  search(evt) {
    evt.preventDefault();
    
    let searchString = (document.querySelector('input[name="searchString"]') as HTMLInputElement).value

    this.omdbService.search(searchString).subscribe(data => {
      console.log(data);
    })

  }

}
