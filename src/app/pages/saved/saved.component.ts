import { Component, OnInit } from '@angular/core';
import { MovieService } from 'src/app/services/movie.service';
import Movie from 'src/app/models/Movie';

@Component({
  selector: 'app-saved',
  templateUrl: './saved.component.html',
  styleUrls: ['./saved.component.scss']
})
export class SavedComponent implements OnInit {

  constructor(private movieService: MovieService) { }

  public movies: Movie[];

  ngOnInit(): void {
    this.movieService.getSavedMovies().subscribe((movies: Movie[]) => {
      this.movies = movies;
    });
  }

  remove(movieId) {
    this.movieService.unsaveMovie(movieId).subscribe(() => {
      this.movies = this.movies.filter(x => x.id !== movieId);
    });
  }
}
