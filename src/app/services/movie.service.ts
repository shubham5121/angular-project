import { Injectable } from '@angular/core';
import Movie from '../models/Movie';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http: HttpClient) { }

  private api = "http://localhost:3000";

  /**
   * Add a movie to the "saved" collection
   * @param movie The movie to save 
   */
  public saveMovie(movie: Movie): Observable<any> {
    return this.http.post(`${this.api}/savedMovies`, movie);
  }

  /**
   * Add a movie to the "seen" collection
   * @param movie The movie to save 
   */
  public seeMovie(movie: Movie): Observable<any> {
    return this.http.post(`${this.api}/seenMovies`, movie);
  }

  /**
   * Remove a movie from the "saved" collection
   * @param id The ID of the movie to remove
   */
  public unsaveMovie(id: number): Observable<any> {
    return this.http.delete(`${this.api}/savedMovies/${id}`)
  }

  /**
   * Remove a movie from the "seen" collection
   * @param id The ID of the movie to remove
   */
  public unseeMovie(id: number): Observable<any> {
    return this.http.delete(`${this.api}/seenMovies/${id}`);
  }

  /**
   * Get a saved movie from the database
   * @param imdbID The IMDB ID of the movie to get
   */
  public getSavedMovie(imdbID: string) {
    return this.http.get<Movie[]>(`${this.api}/savedMovies`, {params: { imdbID, "_limit": "1" }});
  }

  /**
   * Get a seen movie from the database
   * @param imdbId The IMDB ID of the movie to get
   */
  public getSeenMovie(imdbId: string) {
    return this.http.get<Movie[]>(`${this.api}/seenMovies`, { params: { imdbId, }});
  }

  /**
   * Get a list of saved movies
   */
  public getSavedMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(`${this.api}/savedMovies?_sort=id&_order=desc`);
  }

  /**
   * Get a list of seen movies
   */
  public getSeenMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(`${this.api}/seenMovies?_sort=id&_order=desc`);
  }

  /**
   * Get latest saved movies from the database
   * @param limit How many movies to get
   */
  public getLatestSaved(limit: number): Observable<Movie[]> {
    return this.http.get<Movie[]>(`${this.api}/savedMovies?_sort=id&_order=desc&limit=${limit}`);
  }

  /**
   * Get latest seen movies from the database
   * @param limit How many movies to get
   */
  public getLatestSeen(limit: number): Observable<Movie[]> {
    return this.http.get<Movie[]>(`${this.api}/seenMovies?_sort=id&_order=desc&limit=${limit}`);
  }

  /**
   * Gets a list of saved movies to update the local list saved in localStorage
   */
  public updateSavedMovies(): void {
    this.getSavedMovies().subscribe((movies: Movie[]) => {
      const ids = movies.map(movie => movie.imdbID);
      window.localStorage.setItem('savedMovies', JSON.stringify(ids));
    });

    this.getSeenMovies().subscribe((movies: Movie[]) => {
      const ids = movies.map(movie => movie.imdbID);
      window.localStorage.setItem('seenMovies', JSON.stringify(ids));
    })
  }
}
